# PresidentSearch Theme

The theme for the presidentsearch.uconn.edu website.


## Git Workflow (Git Flow)

We use [git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow). Git flow encourages a branching git strategy with several types of branches
- master (for production)
- develop (pre-production)
- feature/* (new features)
- hotfix/* (bug fixes)
- release/* (tagged releases merging into master)

[You can install git flow here](https://github.com/nvie/gitflow/wiki/Installation)

## Development
- Clone the repository.
- `git flow init -d`
- `composer install`
- `docker-compose up`
- `git flow feature start {feature_name}` this starts a new branch
- `git flow feature finish {feature_name}` this merges the branch into `develop`
- test the `develop` branch and share it with others for approval

**See below for instructions on creating hooks for jenkins and bitbucket pipelines for automatic deployment/package management**

## Creating releases
Tags **must** follow the [semver system](http://semver.org/). Follow these steps to complete a release
- on the `develop` branch
  - check `git tag` for the most recent version number
  - bump the version number in `./style.css`
  - add changelog notes to `./changelog.md`
- commit these changes (usually with something like `git -m 'version bump'`)
- follow the process below to create a new tagged release.
```bash
$ git flow release start {new_tag}
$ git flow release finish {new_tag}
$ # in the first screen add new info or just save the commit message as is
$ # in the second screen type in the same tag you just used and save.
$ git push --tags && git push origin master
$ git checkout develop
``` 

## Viewing a project.
This project will be available at [localhost](http://localhost) and has live reloading via [browsersync](https://www.browsersync.io/) for development at [localhost:3000](http://localhost:3000).

CSS and JS assets are shared between the local-dev container and web server. Please see the docker-compose file for how they are mounted/shared.

## Accessing containers
To access a particular docker container, find the container name and then enter an interactive terminal.
```bash
$ docker ps # to get the container name
$ docker exec -it container_name bash
$ docker-compose exec service_name bash
```

## Debugging Wordpress
Wordpress debug logs can be found inside the web container at `/etc/httpd/logs/error_log`

## Bitbucket
### Creating releases
Assuming you're using git flow, tag the release with the command `git flow release start {version_number}`. Tags must follow the [semver system](http://semver.org/). Follow these steps to complete a release
```bash
$ git tag # check the current tags/versions on the project
$ git flow release start {new_tag}
$ git flow release finish {new_tag}
$ # in the first screen add new info or just save the commit message as is
$ # in the second screen type in the same tag you just used and save.
$ git push --tags && git push origin master
$ git checkout develop
``` 
Finally re-run the pipeline build on the [satis repo](https://bitbucket.org/ucomm/composer-repository).
### Pipelines
This repo has a bitbucket pipelines (written by Adam Berkowitz) attached in case you wish to create zip file downloads for tags/branches. **You may exclude files/folders from the zip archive by adding them to composer.json. The syntax is the same as gitignore. You may explicitly include files/directories as well (e.g. !.gitignore, !vendor/*).** To enable pipelines on bitbucket, simply visit the project repo and enable pipelines at `repo -> settings -> Pipelines/settings`.

### Access Keys
Many projects require integration with either Jenkins (below), or our Satis/composer package repo. These require ssh keys. To add keys to a project...
- Find another project with keys (castor is a good example)
- In that project, go to `Settings -> General -> Access Keys` 
- Copy the keys you need
- In the project you're working on, add new keys (typically called Jenkins and Composer for clarity)

## Jenkins
In order to ensure automatic pushes to our development server(s) take the following steps
- Create a new [Jenkins project](https://ci.pr.uconn.edu:8083/) (either from scratch or by copying - see the [Castor project](http://ci.pr.uconn.edu:8080/job/Castor%20-%20Push%20to%20Dev%20(Aurora%20Sandbox)/) for an example)
- In bitbucket settings, go to `Settings -> Workflow -> Webhooks` and add a hook to Jenkins at the url `http://ci.pr.uconn.edu:8080/bitbucket-hook/`.
- In bitbucket settings, go to `Settings -> General -> Access Keys` and add the Jenkins ssh key. The key can be copy/pasted from another repo
- **If this project is going to be deployed to either the Aurora sandbox or health dev servers, make a new directory with the same name as the project using an ftp client. Otherwise the deployment may fail.**
