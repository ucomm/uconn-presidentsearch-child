<?php

// Constants
define( 'UCONN_PRESIDENTSEARCH_CHILD_DIR', get_stylesheet_directory() );
define( 'UCONN_PRESIDENTSEARCH_CHILD_URL', get_stylesheet_directory_uri() );

// Load
if (file_exists(dirname(ABSPATH) . '/vendor/autoload.php')) {
  require_once(dirname(ABSPATH) . '/vendor/autoload.php');
} elseif (file_exists(ABSPATH . 'vendor/autoload.php')) {
  require_once(ABSPATH . 'vendor/autoload.php');
} else {
  require_once('vendor/autoload.php');
}
require_once( 'lib/ScriptLoader.php' );

// Enqueue Scripts and Styles
add_action( 'wp_enqueue_scripts', array('PresidentChild\Lib\ScriptLoader', 'load_scripts') );