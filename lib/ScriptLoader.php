<?php

namespace PresidentChild\Lib;

class ScriptLoader {

    public static function load_scripts() {

        wp_register_style('uconn-presidentsearch-theme-styles', get_template_directory_uri() . '/build/main.css');

        wp_register_style('uconn-presidentsearch-child-styles', get_stylesheet_directory_uri() . '/build/main.css', ['uconn-presidentsearch-theme-styles']);

        wp_register_style('uconn-banner', get_stylesheet_directory_uri() . '/vendor/uconn/banner/_site/banner.css');

        wp_enqueue_style('uconn-presidentsearch-theme-styles');
        wp_enqueue_style('uconn-presidentsearch-child-styles');
        wp_enqueue_style('uconn-banner');
    }

}